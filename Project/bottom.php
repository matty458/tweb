<!--
  Mattia Salasso Tweb 2018-19
  footer della pagina incluso in tutte le altre pagine
-->
    <div id="footer-container" class="container-fluid fixed-bottom">
      <nav class="navbar navbar-expand-sm navbar-color">
        <!-- Brand -->
        <a class="navbar-brand" href="index.php">
          <img src="IMG/logo.png" alt="logo">
        </a>

        <ul class="navbar-nav ml-auto">
          <li><a href="ttp://validator.w3.org/check/referer"><img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML"></a></li>
          <li><a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a></li>
        </ul>
      </nav>
    </div><!-- #footer-container -->

  </body>
</html>
