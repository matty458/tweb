<!--
  Mattia Salasso Tweb 2018-19
  pagina di registrazione al sito
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  if(!isset($_SESSION["email"])){ ?>
    <div id="login-signup">
      <form id="buttonControl" action="server/signup-submit.php" method="post">
        <h2>Crea il tuo account</h2>
        <div class="form-group">
          <label>Indirizzo e-mail</label>
          <input type="email" class="form-control" name="email" placeholder="Email" required>
          <?php
            if(isset($_SESSION["errormail"])){ ?>
              <small id="errorMail" class="form-text text">E-mail già registrata</small><?php
              unset($_SESSION["errormail"]);
            }
          ?>
        </div>

        <div class="form-group">
          <label>Username</label>
          <input id="user" type="text" class="form-control" name="username" placeholder="Username" required>
          <small class="form-text text-muted">Inserire un username di almeno 5 caratteri</small>
        </div>

        <div id="error" class="form-group">
          <label>Password</label>
          <input id="password" type="password" maxlength="16" class="form-control" name="password" placeholder="Password" required>
          <small class="form-text text-muted">La password deve essere compresa tra 8 e 16 caratteri e non deve contenere caratteri speciali</small>
        </div>

        <div id="error1" class="form-group">
          <label>Ripeti Password</label>
          <input id="passwordRepeat" type="password" maxlength="16" name="repeatPassword" class="form-control" placeholder="Ripeti Password" required>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Crea account</button>
        </div>

        <div class="form-group">
          <a href="login.php">Hai già un account? Effettua il login</a>
        </div>
      </form><!-- #buttonControl -->
    </div><?php #login-signup
  }else{ ?>
    <div class="container">
      <h1>Sei già registrato</h1>
      <a class="nav-link" href="showAllProduct.php">Inizia la ricerca di articoli</a>
    </div><?php
  }
?>

<script src="JS/signup.js"></script>

<?php include("bottom.php"); ?>
