<!--
  Mattia Salasso Tweb 2018-19
  header della pagina incluso in tutte le altre pagine
-->

<?php include("CONFIG/function.php"); ?>
<!DOCTYPE html>
<html lang=en>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Progetto Tweb 2018-19</title>

    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css"><!-- bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!-- per i loghi -->
    <link rel="stylesheet" href="CSS/progetto.css">

    <script src="JS/ajax_util.js"></script>
    <script src="JS/prototype.js"></script>
    <script src="scriptaculous-js-1.9.0/src/scriptaculous.js"></script>
    <script src="JS/function.js"></script>
    <script src="JS/add.js"></script>
    <script src="JS/remove.js"></script>
    <script src="JS/carrello.js"></script>
    <script src="JS/dropdown.js"></script>
    <script src="JS/loadScript.js"></script>
  </head>
  <body>

    <?php if(!isset($_SESSION)){ session_start(); } ?>

    <div id="header-container" class="container-fluid fixed-top">
      <nav class="navbar navbar-expand-lg navbar-color">
        <a class="navbar-brand" href="index.php">
          <img src="IMG/logo.png" alt="logo">
        </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="showAllProduct.php">Tutte le categorie</a></li>

            <li class="nav-item">
              <a id="dropdown" class="nav-link dropdown-toggle" href="#">
                <?php
                  if(isset($_SESSION["email"])){ ?>
                    Benvenuto <?php
                  }else{ ?>
                    Account <?php
                  }
                ?>
              </a>
              <div id="myDropdown" class="dropdown-menu dropdown-content">
              <?php
                if(!isset($_SESSION["email"])){ ?>
                  <a class="dropdown-item" href="login.php">Login</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="signup.php">Registrazione</a><?php
                }else{ ?>
                  <a class="dropdown-item" href="server/logout.php">Esci</a><?php
                }
              ?>
              </div>
            </li>
          </ul>

          <div class="my-2 my-lg-0">
            <button id="truck" class="btn btn-lg btn-block">
              <i id="carrello" class="fa fa-cart-plus fa-lg logo"></i>
              <span>Il tuo carrello</span>
            </button>
          </div>
        </div>
      </nav><!-- .navbar navbar-expand-lg navbar-color -->

      <div id="mySidenav" class="sidenav">
        <a id="close" href="javascript:void(0)" class="closebtn">&times;</a>
        <div id="card-container" class="container-fluid">
        <?php
          if(isset($_SESSION["truck"])){ ?>
            <h1>I tuoi articoli</h1><?php
            $db = connectToDatabase();
            for($i=0; $i<count($_SESSION["truck"]); $i++){
              $id = $_SESSION["truck"][$i];
              $rows = articlesSelected($id);
              foreach($rows as $lines){ ?>
                <div class="card">
                  <img src=<?= $lines["image"] ?> class="card-img-top" alt="image card">
                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><h4 class="card-title type"><?= $lines["type"] ?></h4></li>
                      <li class="list-group-item"><h5 class="card-title"><?= $lines["brand"] ?></h5></li>
                      <li class="list-group-item"><p class="card-text"><?= $lines["description"] ?></p></li>
                      <li class="list-group-item"><p class="card-text price"><?= $lines["price"] ." €" ?></p></li>
                    </ul>
                  </div>
                  <div class="card-footer">
                    <a class="btn btn-danger view">Rimuovi Articolo</a>
                    <input type="hidden" name="id" value=<?= $lines["id"] ?>>
                  </div>
                </div><?php
              }
            }
          }else{ ?>
            <h4>Nessun articolo nel carrello</h4><?php
          }
        ?>
          <button id="buy" type="button" class="btn btn-success d-none">Procedi con l'ordine</button>
        </div><!-- #card-container -->
      </div><!-- #mySidenav -->
    </div><!-- #header-container -->
