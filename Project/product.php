<!--
  Mattia Salasso Tweb 2018-19
  pagina dove si vede l'articolo in dettaglio ed una lista di articoli simili
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  $id = $_POST["id"];
  $image = $_POST["image"];
  $type = $_POST["type"];
  $brand = $_POST["brand"];
  $description = $_POST["description"];
  $price = $_POST["price"];
  $qty = $_POST["qty"];
  $rating = $_POST["rating"];
  $views = $_POST["views"];
  $views++;
  increaseViews($views,$description);
?>

<div class="container-fluid">
  <h1>Stai guardando la categoria: <?= $type ?></h1>
  <div class="row">
    <div class="col-sm-6 left">
      <div class="product-img">
        <img src=<?= $image ?> alt="product image">
      </div>
    </div>

    <?php
      $split = explode(",",$description); #lista delle Caratteristiche
    ?>

    <div class="col-sm-6 right">
      <ul class="list-group list-group-flush">
        <li class="list-group-item"><h2><?= $split[0] ?></h2></li>
        <li class="list-group-item"><h5>Marca: <span class="label"><?= $brand ?></span></h5></li>
        <li class="list-group-item"><h5>Caratteristiche:</h5>
          <ul class="list-group list-group-flush">
            <?php
              for($k=1; $k<count($split); $k++){ ?>
                <li class="list-group-item"><?= $split[$k] ?></li><?php
              }
            ?>
          </ul>
        </li>
        <li class="list-group-item"><h5>Prezzo: <span class="label"><?= $price ." €" ?></span></h5></li>
        <li class="list-group-item"><h5>Quantità disponibile: <span class="label"><?= $qty ?></span></h5></li>
        <li class="list-group-item">
          <h5>Gli altri utenti hanno valutato l'articolo:
          <?php
            for($i=0; $i<$rating; $i++){ ?>
              <i class="fa star">&#xf005;</i><?php
            }
          ?>
          </h5>
        </li>
        <li class="list-group-item"><h5>Questo articolo è stato visualizzato:<span class="label"><?= $views ." volte"?></span></h5>
        <li class="list-group-item">
          <a class="btn btn-primary view detail">Aggiungi al carrello</a>
          <input type="hidden" value=<?= $id; ?>>
        </li>
      </ul>
    </div>
  </div><!-- .row -->
</div><!-- .container-fluid -->

<div class="container-fluid"><!-- Articoli simili -->
  <h1 class="list-articles">Confronta con articoli simili</h1>
  <div id="mainTable">
    <div class="row">
    <?php
      $i=0;
      $rows = similarArticles($type,$brand);
      foreach($rows as $line){
        if(strcmp($line["image"],$image) != 0 && $i<3){ ?> <!--non ripete il prodotto che si vede sopra,max 3 prodotti -->
          <div class="col-sm-4">
            <div class="container-fluid container-product">
              <img src=<?= $line["image"]; ?> alt="product image">
              <div class="container list-product">
                <ul class="list-group">
                  <li class="list-group-item">Marca:<span class="label"><?= $line["brand"]; ?></span></li>
                  <li class="list-group-item">Descrizione:<span class="label"><?= $line["description"]; ?></span></li>
                  <li class="list-group-item">Quantità disponibile:<span class="label"><?= $line["qty"]; ?></span></li>
                  <li class="list-group-item">Prezzo:<span class="label"><?= $line["price"] ." €"; ?></span></li>
                  <li class="list-group-item">Rating:
                    <?php
                      for($j=0; $j<$line["rating"]; $j++){ ?>
                        <i class="fa star">&#xf005;</i><?php
                      }
                    ?>
                  </li>
                </ul>

                <form class="list-form" action="product.php" method="post">
                  <input type="hidden" name="id" value="<?= $line["id"]; ?>" >
                  <input type="hidden" name="image" value="<?= $line["image"]; ?>" >
                  <input type="hidden" name="type" value="<?= $line["type"]; ?>" >
                  <input type="hidden" name="description" value="<?= $line["description"]; ?>" >
                  <input type="hidden" name="brand" value="<?= $line["brand"]; ?>" >
                  <input type="hidden" name="price" value="<?= $line["price"]; ?>" >
                  <input type="hidden" name="qty" value="<?= $line["qty"]; ?>" >
                  <input type="hidden" name="rating" value="<?= $line["rating"]; ?>" >
                  <input type="hidden" name="views" value="<?= $line["views"]; ?>" >

                  <button type="submit" class="btn btn-primary">Visualizza</button>
                  <a class="btn btn-primary view list">Aggiungi al carrello</a>
                </form>
              </div>
            </div>
          </div><?php
          $i++;
        }
      }
    ?>
  </div><!-- .row -->
  </div><!-- #mainTable -->
</div><!-- .container-fluid -->

<?php include("bottom.php"); ?>
