var globalId; // salva l'id dell'articolo da aggiungere,usato nei metodi di onSuccess delle richieste ajax

function addProduct(){ // Aggiunge al carrello un prodotto
  var card = this.up(".card");
  var cardClone = card.clone(true);

  var id = cardClone.querySelector("input").value;
  globalId = id;
  addSession(id);
}

function addSession(id){ // Aggiunge a S_SESSION["truck"] l'id dell'articolo, solo se la quantità di quell'articolo è disponibile
  new Ajax.Request("XML/truck.php",
      {
          method: "POST",
          parameters: {article: id},
          onSuccess: addItemIntoTruck,
          onFailure: ajaxFailed,
          onException: ajaxFailed
      }
  );
}

function addItemIntoTruck(ajax){ // Aggiunge l'articolo al carrello clonando la carta su showAllProduct.php
  var article = ajax.responseXML.getElementsByTagName("message");

  if(article[0].firstChild.nodeValue == "add"){
    for(var i=0; i<$$(".allCardDeck .card").length; i++){
      if($$(".allCardDeck .card")[i].querySelector("input").value == globalId){
        var clone = $$(".allCardDeck .card")[i].clone(true);

        var card_footer = document.createElement("div");
        card_footer.className = "card-footer";

        var button_remove = document.createElement("a");
        button_remove.className = "btn btn-danger view";
        button_remove.innerHTML = "Rimuovi Articolo";

        button_remove.observe("click",removeItem);

        var inputId = document.createElement("input");
        inputId.type = "hidden";
        inputId.name = "id";
        inputId.value = globalId;

        card_footer.appendChild(button_remove);
        card_footer.appendChild(inputId);

        clone.appendChild(card_footer);
        clone.getElementsByClassName("card-footer")[0].remove();

        checkTruck();

        $("card-container").insertBefore(clone,$("buy"));

        showInfoTruck("add");
      }
    }
  }else if(article[0].firstChild.nodeValue == "userNotFound"){
    showInfoTruck("userNotFound");
  }else{
    showInfoTruck("qtyNotFound");
  }
}

function addProductList(){ // Aggiunge al carrello il prodotto(Solo se cliccato dalla lista articoli simili)
  var id = this.up("form").querySelector("input").value;
  globalId = id;
  addSessionAll(id);
}

function addProductDetail(){ // Aggiunge al carrello il prodotto(Solo se cliccato dal prodotto visualizzato in dettaglio)
  var id = this.next().value;
  globalId = id;
  addSessionAll(id);
}

function addSessionAll(id){ // Aggiunge a S_SESSION["truck"] l'id dell'articolo, solo se la quantità di quell'articolo è disponibile
  new Ajax.Request("XML/truck.php",
      {
          method: "POST",
          parameters: {article: id},
          onSuccess: addItemIntoTruckAll,
          onFailure: ajaxFailed,
          onException: ajaxFailed
      }
  );
}

function addItemIntoTruckAll(ajax){ // Aggiunge il prodotto al carrello(dalla pagina product.php), o mostra un errore se la qty non è disponibile
  var article = ajax.responseXML.getElementsByTagName("message");

  if(article[0].firstChild.nodeValue == "add"){
    requestArticle(globalId);
  }else if(article[0].firstChild.nodeValue == "userNotFound"){
    showInfoTruck("userNotFound");
  }else{
    showInfoTruck("qtyNotFound");
  }
}

function requestArticle(id){ // Richiede i dati in formato XML dell'articolo con id passato come parametro
  new Ajax.Request("XML/requestArticle_xml.php",
      {
          method: "GET",
          parameters: {id_article: id},
          onSuccess: addArticle,
          onFailure: ajaxFailed,
          onException: ajaxFailed
      }
  );
}

function addArticle(ajax){ // Crea la carta

  var article = ajax.responseXML.getElementsByTagName("article");

  var card = document.createElement("div");
  card.className = "card";

  var img = document.createElement("img");
  img.className = "card-img-top";
  var pathNode = article[0].getElementsByTagName("path")[0];
  var path = pathNode.firstChild.nodeValue;
  img.setAttribute("src", path);

  var card_body = document.createElement("div");
  card_body.className = "card-body";

  var ul = document.createElement("ul");
  ul.className = "list-group list-group-flush";

  var li_title = document.createElement("li");
  li_title.className = "list-group-item";

  var li_brand = document.createElement("li");
  li_brand.className = "list-group-item";

  var li_paragraph = document.createElement("li");
  li_paragraph.className = "list-group-item";

  var li_price = document.createElement("li");
  li_price.className = "list-group-item";

  var li_rating = document.createElement("li");
  li_rating.className = "list-group-item";

  var title = document.createElement("h5");
  title.className = "card-title";
  var type = article[0].getAttribute("category");
  title.innerHTML = type;

  var brand = document.createElement("h5");
  brand.className = "card-title";
  var brandType = article[0].getAttribute("brand");
  brand.innerHTML = brandType;

  var paragraph = document.createElement("p");
  paragraph.className = "card-text";
  var description = article[0].getAttribute("description");
  paragraph.innerHTML = description;

  var price = document.createElement("p");
  price.className = "card-text price";
  price.innerHTML = article[0].getAttribute("price") + "  €";

  var rating = document.createElement("p");
  rating.className = "card-text rating";
  rating.id = "rate";
  var ratingNode = article[0].getElementsByTagName("rate")[0];
  var ratingValue = ratingNode.firstChild.nodeValue;

  for(var j=0; j<ratingValue; j++){
    var star = document.createElement("i");
    star.className = "fa star";
    star.innerHTML = "&#xf005;";
    rating.appendChild(star);
  }

  var card_footer = document.createElement("div");
  card_footer.className = "card-footer";

  var button_remove = document.createElement("a");
  button_remove.className = "btn btn-danger view";
  button_remove.innerHTML = "Rimuovi Articolo";

  button_remove.observe("click",removeItem);

  var inputId = document.createElement("input");
  inputId.type = "hidden";
  inputId.name = "id";
  inputId.value = article[0].getAttribute("id");

  card.appendChild(img);

  card.appendChild(card_body);
    card_body.appendChild(ul);
      ul.appendChild(li_title);
      ul.appendChild(li_brand);
      ul.appendChild(li_paragraph);
      ul.appendChild(li_price);
      ul.appendChild(li_rating);
      li_title.appendChild(title);
      li_brand.appendChild(brand);
      li_paragraph.appendChild(paragraph);
      li_price.appendChild(price);
      li_rating.appendChild(rating);

  card.appendChild(card_footer);
    card_footer.appendChild(button_remove);
    card_footer.appendChild(inputId);

    checkTruck();

    $("card-container").insertBefore(card,$("buy"));

    showInfoTruck("add");

}
