function truck(){ //listener del logo carrello e sul button "PROCEDI ALL'ACQUISTO"
  $("truck").observe("click",openNav);
  $("buy").observe("click",redirect);
}

function openNav(event){ // apertura carrello
  $("mySidenav").style.width = "350px";
  $("close").onclick = closeNav;
}

function closeNav() { // chiusura carrello, cliccando sulla x
  $("mySidenav").style.width = "0";
}

function redirect(){ // Porta alla pagina acquisto e pagamento
  window.location = "shop.php";
}

function loadCart(){ // controllo del carrello al caricamento della pagina
  if($$("#card-container .card") != 0){
    $("buy").className = "btn btn-success";
  }
}

function addListenerCardDraggable(){ // Le card a sinistra del cestino possono essere trascinate
  var cards = $$(".allCardDeck .card");
  for(var i=0; i<cards.length; i++){
    new Draggable(cards[i],{revert: true});
  }
}

function addListenerTrash(){ // Il cestino dove è possibile rilasciare le card Draggable
  Droppables.add($("trash"),
    {
      onDrop: dropItem
    });
}

function dropItem(item){ //cancella l'item trascinato , anche dal carrello
  var id_remove = item.querySelector("input").value;
  removeSession(id_remove,"removeSession");
  removeArticleTruck(id_remove);
  item.remove();
  checkPrice();
  loadTruck();
  showInfoTruck("delete");
  checkNumArticles();
}
