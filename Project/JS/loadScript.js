window.onload = scriptLoad;

function scriptLoad(){ // carica tutti gli script
  showAccount(); // script del dropdown menu dell'account
  truck(); // script del carrello(logo)
  addListener(); // listener dei button "aggiungi al carrello"
  loadCart(); // controllo del carrello al caricamento della pagina

  if($("payment-button-amount") != null){ // Se sono in shop.php
    addListenerRemoveArticle(); // listener dei button "rimuovi" sulla pagina shop.php
    addListenerCardDraggable(); // listener delle card che possono essere trascinate
    addListenerTrash(); // listener del cestino
  }else{
    addListenerTruck(); // listener dei button "rimuovi" sulle restanti pagine
  }

}

function addListener(){ // Aggiunge il listener ad ogni button che aggiunge al carrello
  for(var k=0; k<$$(".list").length; k++){ // articolo sulla lista "articoli simili"
    $$(".list")[k].observe("click",addProductList);
  }

  for(var j=0; j<$$(".add").length; j++){ // articolo
    $$(".add")[j].observe("click",addProduct);
  }

  for(var i=0; i<$$(".detail").length; i++){ //articolo in dettaglio
    $$(".detail")[i].observe("click",addProductDetail);
  }

}

function addListenerRemoveArticle(){ // associa il listener ai button della pagina shop.php
  for(var i=0; i<$$(".view").length; i++){
    $$(".view")[i].observe("click",removeArticle);
  }
  $("removeAll").observe("click",removeAll);
}

function addListenerTruck(){ // Aggiunge il listener ai button del carrello creati dinamicamente
  for(var i=0; i<$("card-container").querySelectorAll("a").length; i++){
    $("card-container").querySelectorAll("a")[i].observe("click",removeItem);
  }
}
