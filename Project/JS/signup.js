$("login-signup").observe("submit",clientSignUpControl);

function clientSignUpControl(event){
  if(userControl(event) && passwordControl(event) && passwordRepeatControl(event)){
    verify(event);
  }
}

function checkError(){
  if($("errorMail") != null){
    $("errorMail").remove();
  }
  if($("errorUser") != null){
    $("errorUser").remove();
  }
  if($("errorPassword") != null){
    $("errorPassword").remove();
  }
  if($("errorPasswordRepeat") != null){
    $("errorPasswordRepeat").remove();
  }
}

function userControl(event){
  checkError();
  if($("user").value.includes("<") || $("user").value.includes(">")){
    var errorUser = document.createElement("small");
    errorUser.id = "errorUser";
    errorUser.className = "form-text text";
    errorUser.innerHTML = "Non inserire '<' o '>' nel campo username";
    $("user").parentNode.appendChild(errorUser);
    $("user").clear();
    event.preventDefault();
    return false;
  }
  return true;
}

function passwordControl(event){
  checkError();
  if($("password").value.includes("<") || $("password").value.includes(">")){
    var errorPassword = document.createElement("small");
    errorPassword.id = "errorPassword";
    errorPassword.className = "form-text text";
    errorPassword.innerHTML = "Non inserire '<' o '>' nel campo password";
    $("password").parentNode.appendChild(errorPassword);
    $("password").clear();
    event.preventDefault();
    return false;
  }
  return true;
}

function passwordRepeatControl(event){
  checkError();
  if($("passwordRepeat").value.includes("<") || $("passwordRepeat").value.includes(">")){
    var errorPasswordRepeat = document.createElement("small");
    errorPasswordRepeat.id = "errorPasswordRepeat";
    errorPasswordRepeat.className = "form-text text";
    errorPasswordRepeat.innerHTML = "Non inserire '<' o '>' nel campo ripeti password";
    $("passwordRepeat").parentNode.appendChild(errorPasswordRepeat);
    $("passwordRepeat").clear();
    event.preventDefault();
    return false;
  }
  return true;
}

function verify(event){
  if($("user").value.length < 5){
    var userError = document.createElement("small");
    userError.id = "errorUser";
    userError.className = "form-text text";
    userError.innerHTML = "username troppo corto";
    $("user").parentNode.appendChild(userError);
    $("user").clear();
    event.stop();
  }else{
    if(!(($("password").value.length >= 8) && ($("password").value.length <= 16))){
      if($("errorPassword") != null){
        $("errorPassword").remove();
      }
      if($("errorPasswordRepeat") != null){
          $("errorPasswordRepeat").remove();
      }
      var errorPassword = document.createElement("small");
      errorPassword.id = "errorPassword";
      errorPassword.className = "form-text text";
      errorPassword.innerHTML = "Errore lunghezza password";
      $("error").appendChild(errorPassword);
      $("password").clear();
      $("passwordRepeat").clear();
      event.stop();
    }else{
      if($("password").value != $("passwordRepeat").value){
        if($("errorPasswordRepeat") != null){
            $("errorPasswordRepeat").remove();
        }
        if($$(".text").length != 0){
          $("errorPassword").remove();
        }
        var error = document.createElement("small");
        error.id = "errorPasswordRepeat";
        error.className = "form-text text";
        error.innerHTML = "password diverse";
        $("error1").appendChild(error);
        $("passwordRepeat").clear();
        event.stop();
      }
    }
  }
}
