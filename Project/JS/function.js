function remove(){ // Rimuove tutti i card-deck
  var elemRemove = $$("#showAll .card-deck");
  var remove = elemRemove;
  for(var i=0; i<elemRemove.length; i++) {
    remove[i].remove();
  }
}

function cardDeckAppend(){ // Crea un elemento <div id="append"> dove vengono poi appesi come figli i card-deck
  if($("append") != null){
    $("append").remove();
  }
  var cardAppend = document.createElement("div");
  cardAppend.id = "append";
  $("showAll").appendChild(cardAppend);
}

function checkError(){ // Cancella "Nessun Articolo Trovato nel caso vengano effettuate piu ricerche"
  if($$("#showAll #errorArticle") != 0){
    $("errorArticle").remove();
  }
}

function checkTruck(){ // Controlla il carrello
  if($$("#card-container .card") != 0){
    $("buy").className = "btn btn-success";
  }else{
    $("card-container").querySelector("h4").remove();
    var myTruck = document.createElement("h1");
    myTruck.innerHTML = "I tuoi articoli";
    $("card-container").insertBefore(myTruck,$("buy"));
    $("buy").className = "btn btn-success";
  }
}

function showInfoTruck(option){ // Mostra all'utente un informazione quando aggiunge o rimuove un articolo
  var infoAppend = document.createElement("li");
  infoAppend.id = "info";
  infoAppend.className = "nav-item";

  if($("info") != null){
    $("info").remove();
  }

  $$("#header-container .navbar-nav")[0].appendChild(infoAppend);

  var text = document.createElement("a");
  text.className = "nav-link";

  switch(option){
    case "add":
      text.innerHTML = "Articolo aggiunto con successo";
      break;
    case "delete":
      text.innerHTML = "Articolo rimosso con successo";
      break;
    case "deleteAll":
      text.innerHTML = "Tutti gli articoli rimossi con successo";
      break;
    case "success":
      text.innerHTML = "Acquisto effettuato,sarai indirizzato alla home page tra 3 secondi";
      break;
    case "qtyNotFound":
      text.innerHTML = "Quantità non disponibile,articolo non aggiunto";
      break;
    case "userNotFound":
      text.innerHTML = "Registrati prima di poter aggiungere articoli nel carrello";
      break;
  }

  $("info").appendChild(text);

  if(option == "success"){
    setTimeout(function(){
      $("info").getElementsByTagName("a")[0].innerHTML = "Acquisto effettuato,sarai indirizzato alla home page tra 2 secondi";
    },1000);

    setTimeout(function(){
      $("info").getElementsByTagName("a")[0].innerHTML = "Acquisto effettuato,sarai indirizzato alla home page tra 1 secondo";
    },2000);

    setTimeout(function(){
      window.location = "index.php";
    },3000);
  }else{
    setTimeout(function(){
      $("info").remove();
    },1000);
  }

}

function createCard(products){ // Crea le card dei prodotti

    var card = document.createElement("div");
    card.className = "card";

    var img = document.createElement("img");
    img.className = "card-img-top";
    var pathNode = products.getElementsByTagName("path")[0];
    var path = pathNode.firstChild.nodeValue;
    img.setAttribute("src", path);
    img.alt = "Card image";

    var card_body = document.createElement("div");
    card_body.className = "card-body";

    var ul = document.createElement("ul");
    ul.className = "list-group list-group-flush";

    var li_title = document.createElement("li");
    li_title.className = "list-group-item";

    var li_brand = document.createElement("li");
    li_brand.className = "list-group-item";

    var li_paragraph = document.createElement("li");
    li_paragraph.className = "list-group-item";

    var li_price = document.createElement("li");
    li_price.className = "list-group-item";

    var li_qty = document.createElement("li");
    li_qty.className = "list-group-item";

    var li_rating = document.createElement("li");
    li_rating.className = "list-group-item";

    var title = document.createElement("h4");
    title.className = "card-title";
    var type = products.getAttribute("category");
    title.innerHTML = type;

    var brand = document.createElement("h5");
    brand.className = "card-title type";
    var brandType = products.getAttribute("brand");
    brand.innerHTML = brandType;

    var paragraph = document.createElement("p");
    paragraph.className = "card-text";
    var description = products.getAttribute("description");
    paragraph.innerHTML = description;

    var price = document.createElement("p");
    price.className = "card-text price";
    price.innerHTML = products.getAttribute("price") + "  €";

    var qty = document.createElement("p");
    qty.className = "card-text";
    var qtyNode = products.getElementsByTagName("quantity")[0];
    var qtyValue = qtyNode.firstChild.nodeValue;
    qty.innerHTML = qtyValue + " articoli disponibili";

    var rating = document.createElement("p");
    rating.className = "card-text rating";
    rating.id = "rate";
    var ratingNode = products.getElementsByTagName("rate")[0];
    var ratingValue = ratingNode.firstChild.nodeValue;

    for(var j=0; j<ratingValue; j++){
      var star = document.createElement("i");
      star.className = "fa star";
      star.innerHTML = "&#xf005;";
      rating.appendChild(star);
    }

    var footer = document.createElement("div");
    footer.className = "card-footer";

    var form = document.createElement("form");
    form.action = "product.php"
    form.method = "post";

    var buttonView = document.createElement("button");
    buttonView.type = "submit";
    buttonView.className = "btn btn-primary";
    buttonView.innerHTML = "Visualizza";

    var add = document.createElement("a");
    add.className = "btn btn-primary view add";
    add.innerHTML = "Aggiungi al carrello";

    var inputId = document.createElement("input");
    inputId.type = "hidden";
    inputId.name = "id";
    inputId.value = products.getAttribute("id");

    var inputTitle = document.createElement("input");
    inputTitle.type = "hidden";
    inputTitle.name = "type";
    inputTitle.value = type;

    var inputImage = document.createElement("input");
    inputImage.type = "hidden";
    inputImage.name = "image";
    inputImage.value = path;

    var inputBrand = document.createElement("input");
    inputBrand.type = "hidden";
    inputBrand.name = "brand";
    inputBrand.value = brandType;

    var inputDescription = document.createElement("input");
    inputDescription.type = "hidden";
    inputDescription.name = "description";
    inputDescription.value = description;

    var inputPrice = document.createElement("input");
    inputPrice.type = "hidden";
    inputPrice.name = "price";
    inputPrice.value = products.getAttribute("price");

    var inputQty = document.createElement("input");
    inputQty.type = "hidden";
    inputQty.name = "qty";
    inputQty.value = qtyValue;

    var inputRating = document.createElement("input");
    inputRating.type = "hidden";
    inputRating.name = "rating";
    var ratingNode = products.getElementsByTagName("rate")[0];
    var ratingValue = ratingNode.firstChild.nodeValue;
    inputRating.value = ratingValue;

    var inputView = document.createElement("input");
    inputView.type = "hidden";
    inputView.name = "views";
    var viewNode = products.getElementsByTagName("view")[0];
    var viewValue = viewNode.firstChild.nodeValue;
    inputView.value = viewValue;


    $("mainDeck").appendChild(card);
      card.appendChild(img);

      card.appendChild(card_body);
        card_body.appendChild(ul);
          ul.appendChild(li_title);
          ul.appendChild(li_brand);
          ul.appendChild(li_paragraph);
          ul.appendChild(li_price);
          ul.appendChild(li_qty);
          ul.appendChild(li_rating);
          li_title.appendChild(title);
          li_brand.appendChild(brand);
          li_paragraph.appendChild(paragraph);
          li_price.appendChild(price);
          li_qty.append(qty);
          li_rating.appendChild(rating);

      card.appendChild(footer);
        footer.appendChild(form);
          form.appendChild(inputId);
          form.appendChild(inputTitle);
          form.appendChild(inputImage);
          form.appendChild(inputBrand);
          form.appendChild(inputDescription);
          form.appendChild(inputPrice);
          form.appendChild(inputQty);
          form.appendChild(inputRating);
          form.appendChild(inputView);

          form.appendChild(buttonView);
          form.appendChild(add);

}
