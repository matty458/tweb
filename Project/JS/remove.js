function removeItem(){ // Rimuove dinamicamente un articolo dal carrello
  var id_remove = this.next().value;

  removeSession(id_remove,"removeSession");
  removeArticleTruck(id_remove);
  showInfoTruck("delete");
  loadTruck();
}

function removeAll(){ // Rimuove tutti gli articoli, sia dal carrello che dalla pagina(button "rimuovi tutti")
  for(var j=$$(".card").length-1; j>=0; j--){
    if($$(".card")[j].id != "pay"){
      var id_remove = $$(".card")[j].querySelector("input").value;
      removeSession(id_remove,"removeSession");
      $$(".card")[j].remove();
    }
  }
  showInfoTruck("deleteAll");
  loadTruck();
  checkNumArticles();
}

function removeArticle(){ // rimuove un articolo sia dal carrello sia dalla pagina shop.php a seconda di dove viene cliccato
  var id_remove = this.next().value;
  var place = this.up(".card").up().className;
  this.up(".card").remove();
  if(place == "card-deck allCardDeck"){
    for(var j=0; j<$$("#mySidenav .card").length; j++){
      if($$("#mySidenav .card")[j].querySelector("input").value == id_remove){
        $$("#mySidenav .card")[j].remove();
        break;
      }
    }
  }else{
    for(var i=0; i<$$(".card-deck .card").length; i++){
      if($$(".card-deck .card")[i].querySelector("input").value == id_remove){
        $$(".card-deck .card")[i].remove();
        break;
      }
    }
  }

  removeSession(id_remove,"removeSession");
  showInfoTruck("delete");
  checkPrice();
  loadTruck();
  checkNumArticles();
}

function removeArticleTruck(id_remove){ // Rimuove l'articolo dal carrello
  for(var i=0; i<$$("#mySidenav input").length; i++){
    if($$("#mySidenav input")[i].value == id_remove){
      $$("#mySidenav input")[i].up(".card").remove();
      break;
    }
  }
}

function removeSession(id_remove,type){ // Rimuove l'id dalla sessione
  new Ajax.Request("XML/truck.php",
      {
          method: "POST",
          parameters: {article: id_remove,command: type},
          onFailure: ajaxFailed,
          onException: ajaxFailed
      }
  );
}

function loadTruck(){ // Se il carrello ha 0 item, informa l'utente
  if($$("#card-container .card") == 0){
    $("card-container").querySelector("h1").remove();
    $("buy").className = "btn btn-success d-none";
    var info = document.createElement("h4");
    info.innerHTML = "Nessun articolo nel carrello";
    $("card-container").appendChild(info);
  }
}

function checkPrice(){ // Aggiorna il prezzo dinamicamente nella pagina shop
  var prices = $$(".allCardDeck .price");
  var tot_price = 0;
  for(var i=0; i<prices.length; i++){
    tot_price += parseInt(prices[i].innerHTML);
  }
  $("payment-button-amount").innerHTML = tot_price + " €";
}

function checkNumArticles(){ // Controlla il numero di articoli presenti sulla pagina,fa sparire form e cestino nel caso siano 0
  if($$(".allCardDeck .card").length == 0){
    $("container").remove();

    var container = document.createElement("div");
    container.className = "container";

    var h1 = document.createElement("h1");
    h1.innerHTML = "NON HAI NESSUN ARTICOLO NEL CARRELLO";

    var a = document.createElement("a");
    a.className = "nav-link";
    a.href = "showAllProduct.php";
    a.innerHTML = "Vai al catalogo e aggiungi articoli nel carrello";

    $("header-container").after(container);
    container.appendChild(h1);
    container.appendChild(a);

  }
}
