$("search").observe("click",searchProduct); // listener del button "search"

function searchProduct(){ //richiesta al server per vedere se ci sono articoli corrispondenti
  new Ajax.Request("XML/search_xml.php",
      {
          method: "GET",
          parameters: {product: $("name").value},
          onSuccess: showSearchedProduct,
          onFailure: ajaxFailed,     // function shown in previous sections
          onException: ajaxFailed
      }
  );
}

function showSearchedProduct(ajax){ // ricerca un prodotto fra tutti i prodotti salvati sul database
  remove();
  check();

  var products = ajax.responseXML.getElementsByTagName("product");

  cardDeckAppend();

  if(products.length == 0 || products[0].firstChild.nodeValue == "itemNotFound"){
    checkError();
    var error = document.createElement("h1");
    error.id = "errorArticle";
    error.innerHTML = "Nessun Articolo Trovato";
    $("append").appendChild(error);
  }else{
    checkError();
    for(var i=0; i<products.length; i++){
      if(i%3 == 0){
        var card_deck = document.createElement("div");
        card_deck.className = "card-deck allCardDeck";
        card_deck.id = "mainDeck";
        $("append").appendChild(card_deck);
      }

      if(i%3 == 0 && i != 0){
        $("mainDeck").id = "";
      }

      createCard(products[i]);
    }

    addListener();
  }

}
