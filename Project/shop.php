<!--
  Mattia Salasso Tweb 2018-19
  pagina dove si può effettuare l'acquisto dei prodotti scelti
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  if(isset($_SESSION["truck"])){ ?>
    <div id="container" class="container-fluid">
      <h1>Hai selezionato i seguenti articoli:</h1>
      <div class="row">

      <div class="col-sm-6">
        <div class="card-deck allCardDeck">
          <?php
            $tot_price = 0;
            for($i=0; $i<count($_SESSION["truck"]); $i++){
              $id = $_SESSION["truck"][$i];
              $rows = articlesSelected($id);
              if($i%2 == 0 && $i != 0){ ?>
                </div>
                <div class="card-deck allCardDeck"><?php
              }
              foreach($rows as $lines){
                $tot_price+= $lines["price"]; ?>
                <div class="card width">
                  <img src=<?= $lines["image"] ?> class="card-img-top" alt="image card">
                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><h4 class="card-title type"><?= $lines["type"] ?></h4>
                      <li class="list-group-item"><h5 class="card-title"><?= $lines["brand"] ?></h5>
                      <li class="list-group-item"><p class="card-text"><?= $lines["description"] ?></p></li>
                      <li class="list-group-item"><p class="card-text price"><?= $lines["price"] ." €"?></p></li>
                    </ul>
                  </div>
                  <div class="card-footer">
                    <a class="btn btn-danger view">Rimuovi Articolo</a>
                    <input type="hidden" name="id" value="<?= $lines["id"]; ?>">
                  </div>
                </div><?php
              }
            }
          ?>
        </div>
      </div><!-- .col-sm-6 -->

      <div class="col-sm-2">
        <div id="trashScroll" class="container-fluid">
          <h3>Trascina gli articoli che non desideri comprare nel cestino <i class="fa fa-arrow-circle-o-down" style="font-size:24px"></i></h3>

          <div id="trash">
            <img src="IMG/trash.jpg" alt="">
          </div>
          <button id="removeAll" class="btn btn-danger">Rimuovi Tutti
            <i class="fa fa-trash-o trash"></i>
          </button>
        </div>
      </div>

      <div class="col-sm-4">
        <div id="payScroll" class="container-fluid">
          <h2>Acquista:</h2>
          <div id="pay" class="card">
            <div class="card-body">
              <div class="card-title">
                  <h3 class="text-center">Metodo di pagamento</h3>
              </div>
              <hr>
              <div class="form-group text-center">
                <ul class="list-inline">
                    <li class="list-inline-item"><i class="text-muted fa fa-cc-visa fa-2x"></i></li>
                    <li class="list-inline-item"><i class="fa fa-cc-mastercard fa-2x"></i></li>
                    <li class="list-inline-item"><i class="fa fa-cc-amex fa-2x"></i></li>
                    <li class="list-inline-item"><i class="fa fa-cc-discover fa-2x"></i></li>
                </ul>
              </div>
              <div class="form-group has-success">
                <label>Nome sulla carta</label>
                <input id="name" type="text" maxlength="25" class="form-control">
              </div>
              <div class="form-group">
                <label>Numero carta</label>
                <input id="number-card" type="tel" maxlength="16" class="form-control">
                <small class="form-text text-muted">Il numero deve essere composto da 16 cifre</small>
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <label>Scadenza</label>
                    <input id="exspiration" type="tel" class="form-control" placeholder="MM / YY">
                  </div>
                </div>
                <div class="col-6">
                  <label>Codice CVV</label>
                  <div class="form-group">
                    <input id="code" type="tel" maxlength="3" class="form-control">
                    <small class="form-text text-muted">Codice di 3 cifre</small>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Indirizzo di consegna</label>
                <input id="address" type="text" maxlength="25" class="form-control">
                <small class="form-text text-muted">Inserire nome della via e numero civico</small>
              </div>
              <div>
                <button id="pay-button" type="submit" class="btn btn-lg btn-info btn-block">
                  <i class="fa fa-lock fa-lg"></i>&nbsp;
                  <span id="payment-button-amount">Paga <?= $tot_price ." €" ?></span>
                </button>
              </div>
            </div>
          </div><!-- #pay -->
        </div><!-- #payScroll -->
      </div><!-- col-sm-4 -->

    </div><!-- .row -->
  </div><?php #container
}else{ ?>
  <div class="container">
    <h1>NON HAI NESSUN ARTICOLO NEL CARRELLO</h1>
    <a class="nav-link" href="showAllProduct.php">Vai al catalogo e aggiungi articoli nel carrello</a>
  </div><?php
}
?>

<script src="JS/buy.js"></script>

<?php include("bottom.php"); ?>
