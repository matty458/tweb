DROP TABLE if EXISTS products;

CREATE TABLE products (
    id INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type VARCHAR(30) NOT NULL ,
    brand VARCHAR(30) NOT NULL ,
    description VARCHAR(1000) NOT NULL ,
    qty INT(3) NOT NULL ,
    price INT(6) NOT NULL ,
    rating INT(1) NOT NULL ,
    image VARCHAR(100) NOT NULL,
    views INT(4) NOT NULL
) ENGINE = InnoDB;

/* Computer */
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (1,'computer','Asus','X540NAGQ017T Notebook, Display da 15.6, Processore Celeron N3350, 1.1 GHz, HDD da 500 GB, 4 GB di RAM, Nero',9,399,2,'IMG/dbImage/Computer/71u2f6DoypL_SL1500.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (2,'computer','Asus','VivoBook Notebook, Display 15.6 HD LED, Intel Dual Core 64 bit fino a 2.4Ghz, 4GB RAM, Hdd 500GB, Windows 10 PRO',4,299,4,'IMG/dbImage/Computer/41FF1UfBD0L.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (3,'computer','Asus','ZenBook UX430UN-GV030T Ultrabook, Display da 14, Processore i7-8550U fino a 1.8 GHz, SSD da 512 GB, 16 GB DDR3,Blu',3,1299,2,'IMG/dbImage/Computer/71zTo5PkwRL_SL1500.jpg',6);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (4,'computer','Asus','Zenboook UX331UN-EG002T, Monitor 13.3, Intel Core i7-8550U 64 bit fino a 3.4Ghz, RAM 8 GB, SSD da 512 GB, Royal Blue',2,1049,1,'IMG/dbImage/Computer/61HdD1c1oL_SL1000.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (5,'computer','Asus','VivoBook S530UF-BQ281T, Monitor 15.6 FHD No Glare, Intel Core i7-8550U, RAM 8 GB e 16 GB Optane Memory, HDD da 1 TB,Grigio',5,949,3,'IMG/dbImage/Computer/71PBSoCcCL_SL1500.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (6,'computer','Asus','S410UA-BV963T Notebook , Display 14 HD, Intel core i3-7020U, RAM 4 GB DDR4, SSD da 128 GB, Scheda Grafica Integrata, Windows 10',6,499,4,'IMG/dbImage/Computer/81CKTHj9tL_SL1500.jpg',1);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (7,'computer','Asus','Vivobook S510UF-BQ268R Notebook, Monitor 15.6 FHD, Intel Core Core I5-8250U, RAM 8 GB, SSD 256 GB, HDD 1 TB, Gold Metal',7,899,4,'IMG/dbImage/Computer/41dNXBkwYXL.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (8,'computer','Asus','Vivobook Pro UX550GE-BN005R Notebook, Monitor 15.6 FHD No Glare Ips, Intel Core I7-8750H, RAM 16 GB DDR4, SSD da 512 GB',4,1599,4,'IMG/dbImage/Computer/41OFNdmUcqL.jpg',2);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (9,'computer','Asus','D541SA-XO271D notebook/portatile Nero, Computer portatile 39,6 cm (15.6) 1366 x 768 Pixel, 1.6 GHz Intel Celeron N3060',5,299,3,'IMG/dbImage/Computer/81wXnKz4zIL_SL1500.jpg',1);

/* Telephone */
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (10,'telephone','Apple','iPhone XS Max, (64GB), Oro',1,1099,4,'IMG/dbImage/Telephone/61jN8GyULZL_SL1024.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (11,'telephone','Apple','iPhone XR ,(64GB), Nero',3,799,5,'IMG/dbImage/Telephone/51q8XIapLOL_SL1024.jpg',6);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (12,'telephone','Apple','iPhone X ,(64GB), Grigio',4,819,4,'IMG/dbImage/Telephone/51i5ppud0GL_SL1024.jpg',2);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (13,'telephone','Apple','iPhone 8 Plus ,(64GB), Argento',5,819,4,'IMG/dbImage/Telephone/51Wp3QsXiXL_SL1024.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (14,'telephone','Apple','iPhone 7 ,(32GB), Nero',7,479,3,'IMG/dbImage/Telephone/51YrNq7gqWL_SL1024.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (15,'telephone','Apple','iPhone 7 ,(128GB), Oro',9,699,4,'IMG/dbImage/Telephone/517OxQ1FgxL_SL1024.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (16,'telephone','Apple','iPhone 6s ,(128GB), Argento',1,399,3,'IMG/dbImage/Telephone/51bWnboZDkL_SL1024.jpg',1);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (17,'telephone','Apple','iPhone 6s ,(32GB), Oro Rosa',2,299,4,'IMG/dbImage/Telephone/61Kvvjw2iUL_SL1024.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (18,'telephone','Apple','iPhone 6s ,(128GB), Oro',3,549,5,'IMG/dbImage/Telephone/51w79RueGTL_SL1024.jpg',3);

/* TV */
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (19,'tv','Samsung','UE49MU6470S 50 pollici, Smart TV 4K Ultra HD 49, Serie MU6470, Titanio Scuro (2017)',2,449,4,'IMG/dbImage/Tv/8195RsM-fBL_SL1500.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (20,'tv','HISENSE','H55AE6000 52 pollici, TV LED Ultra HD 4K HDR, Super Contrast, Smart TV VIDAA U, Wi-Fi',1,429,5,'IMG/dbImage/Tv/71u2f6DoypL_SL1500.jpg',6);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (21,'tv','Samsung','UE58NU7170UXZT 55 pollici, Smart TV 4 K UHD di 58, Wi -Fi, 3840x2160 pixel, Nero',2,599,4,'IMG/dbImage/Tv/91BwN2+fcyL_SL1500.jpg',7);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (22,'tv','HISENSE','H39AE5500 TV 40 pollici, LED Full HD, Natural Colour, Quad Core, Smart TV VIDAA U, Wi-Fi',6,299,5,'IMG/dbImage/Tv/71okWv17ynL_SL1500.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (23,'tv','HISENSE','H43AE6400 TV 42 pollici, LED Ultra HD 4K HDR, Pure Metal Design, Precision Colour, Wi-Fi',4,399,4,'IMG/dbImage/Tv/61OiCFmm4cL_SL1500.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (24,'tv','HISENSE','H32AE5000 45 pollici, TV LED HD, Natural Colour Enhancer, Clean Sound 12W, Wi-Fi',5,399,4,'IMG/dbImage/Tv/71sI5B4gI3L_SL1500.jpg',1);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (25,'tv','HISENSE','H49N5705 49 pollici, 4K Ultra HD Smart TV, Wi-Fi, Nero, VIDAA U, Grigio',2,499,4,'IMG/dbImage/Tv/71lcZne8vL_SL1440.jpg',2);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (26,'tv','HISENSE','H55N5305 55 pollici, 4K Ultra HD Smart TV, Wi-Fi, Grigio,Quad Core,Natural Colour',4,599,4,'IMG/dbImage/Tv/711FpwtImlL_SL1200.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (27,'tv','HISENSE','H50U7A 50 pollici, TV LED Ultra HD 4K, HDR, Ultra Colour, Super Slim Metal Design',3,699,4,'IMG/dbImage/Tv/81yWLb8SwaL_SL1500.jpg',5);


/* Watches */
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (28,'watches','Apple','Watch Series 2, Cassa 42 mm in Alluminio Grigio Siderale, Cinturino Sport nero',1,249,5,'IMG/dbImage/Watch/517J8WQY6iL_SL1000.jpg',1);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (29,'watches','Apple','Watch Series 4 (GPS), cassa 40 mm in alluminio colore Argento, Cinturino Sport',3,599,4,'IMG/dbImage/Watch/61-A5YrjCHL_SL1024.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (30,'watches','Apple','Watch Series 4 (GPS + Cellular), cassa 40 mm in alluminio Grigio, Cinturino Sport Nero',2,549,5,'IMG/dbImage/Watch/41XUSn-YxJL.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (31,'watches','Apple','Watch Serie 4 (GPS), Cassa 44 mm in Alluminio colore Oro, Cinturino Sport Rosa Conchiglia',2,469,4,'IMG/dbImage/Watch/61tx2RsKaaL_SL1024.jpg',5);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (32,'watches','Apple','Watch Series 4 (GPS), cassa 40 mm in alluminio colore Oro, Cinturino Sport rosa sabbia',3,479,5,'IMG/dbImage/Watch/51IxRalicmL_SL1024.jpg',2);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (33,'watches','Apple','Watch Series 4 (GPS + Cellular), cassa 40 mm in alluminio colore Argento, Cinturino Sport Bianco',2,549,3,'IMG/dbImage/Watch/41v896sFCsL.jpg',3);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (34,'watches','Apple','Watch Series 3 (GPS + Cellular), cassa 38 mm in alluminio Grigio Siderale, Cinturino Sport Nero',5,389,5,'IMG/dbImage/Watch/41q9ZuOTnlL.jpg',4);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (35,'watches','Apple','Watch Series 3 OLED GPS (satellitare), cassa 38 mm in alluminio colore Argento opaco',1,249,5,'IMG/dbImage/Watch/81VA3srnB6L_SL1500.jpg',2);
insert into products(id,type,brand,description,qty,price,rating,image,views,reviews) values (36,'watches','Apple','Watch Series 4 (GPS + Cellular), cassa 40 mm in acciaio colore Oro, Cinturino Sport grigio',5,749,5,'IMG/dbImage/Watch/51WAKUlfQrL.jpg',2);
