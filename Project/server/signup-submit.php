<?php include("../CONFIG/function.php"); ?>

<?php
	if(!isset($_SESSION)){ session_start(); }

	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$password = preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/',$_POST["password"]);
		$user = preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/',$_POST["username"]);
		if(isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["repeatPassword"])
			&& filter_input(INPUT_POST,"email",FILTER_VALIDATE_EMAIL) && strlen($_POST["password"]) >= 8
			&& strlen($_POST["repeatPassword"]) <= 16 && !$password && !$user){
			if(!mailCorrect($_POST["email"])){
			  	signUp($user,$_POST["email"],$password);
					$_SESSION["email"] = $_POST["email"];
					header("Location: ../index.php");
					die;
			}else{
				$_SESSION["errormail"] = "errorMail";
		    header("Location: ../signup.php");
		    die;
			}
		}else{
			die("ERRORE, Presenza di caratteri invalidi nei dati inseriti");
		}
	}else{
		die("ERRORE,il metodo GET non è accettato");
	}
?>
