<?php include("../CONFIG/function.php"); ?>

<?php
  if(!isset($_SESSION)){ session_start(); }

  if($_SERVER["REQUEST_METHOD"] == "POST"){
    $password = preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/',$_POST["password"]);
    if(isset($_POST["email"]) && isset($_POST["password"]) && filter_input(INPUT_POST,"email",FILTER_VALIDATE_EMAIL) && !$password){
      if(mailCorrect($_REQUEST["email"]) && strlen($_POST["password"]) >= 8 && strlen($_POST["password"]) <= 16){
        $mail = $_POST["email"];
        if(passwordCorrect($mail,$_POST["password"])){
            if(isset($_SESSION)){
              session_regenerate_id(TRUE);
            }
            $_SESSION["email"] = $mail;
            header("Location: ../index.php");
            die;
        }else{
          $_SESSION["errorpassword"] = "error";
          header("Location: ../login.php");
          die;
        }
      }else{
        $_SESSION["errormail"] = "errorMail";
        header("Location: ../login.php");
        die;
      }
    }else{
      die("ERRORE, Presenza di caratteri invalidi nei dati inseriti");
    }
  }else{
    die("ERRORE,il metodo GET non è accettato");
  }
?>
