<?php
  if(!isset($_SESSION)){ session_start(); }
  include("../CONFIG/config.php");

  # Removes all characters except letters/numbers from query parameters
  function filter_chars($str) {
    return preg_replace("/[^A-Za-z0-9_]*/", "", $str);
  }

  # main program
  if (!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"] != "POST") {
    header("HTTP/1.1 400 Invalid Request");
    die("ERROR 400: Invalid request - This service accepts only POST request");
  }

  $article = "";
  $command = "";

  if (isset($_REQUEST["article"])) {
    $article = filter_chars($_REQUEST["article"]);
  }

  if (isset($_REQUEST["command"])) {
    $command = filter_chars($_REQUEST["command"]);
  }

  header("Content-type: application/xml");
  print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

if(isset($_SESSION["email"])){
  if($article) {
    if(strcmp($command,"removeSession") == 0){
      for($i=0; $i<count($_SESSION["truck"]); $i++){
        if($_SESSION["truck"][$i] == $article){
          unset($_SESSION["truck"][$i]);
          $_SESSION["truck"] = array_values($_SESSION["truck"]);
          break;
        }
      }
      if(count($_SESSION["truck"]) == 0){
        unset($_SESSION["truck"]);
      }
    }else{
      if(isset($_SESSION["truck"])){
        $count = 0;
        for($i=0; $i<count($_SESSION["truck"]); $i++){
          if($_SESSION["truck"][$i] == $article){
            $count++;
          }
        }

        $db = connectToDatabase();
        $articleQuote = $db->quote($article);
        $query = "SELECT qty FROM products WHERE id = $articleQuote";
        $rows = $db->query($query);
        foreach($rows as $lines){
          if($lines["qty"] <= $count){
            $error = "qtyNotFound";
            print "<items>\n";
              print "\t<message>".$error."</message>\n";
            print "</items>\n";
          }else{
            array_push($_SESSION["truck"],$article);
            $error = "add";
            print "<items>\n";
              print "\t<message>".$error."</message>\n";
            print "</items>\n";
          }
        }
      }else{
        $db = connectToDatabase();
        $articleQuote = $db->quote($article);
        $query = "SELECT qty FROM products WHERE id = $articleQuote";
        $rows = $db->query($query);

        foreach($rows as $lines){
          if($lines["qty"] == 0){
            $error = "qtyNotFound";
            print "<items>\n";
              print "\t<message>".$error."</message>\n";
            print "</items>\n";
          }else{
            $_SESSION["truck"] = [];
            array_push($_SESSION["truck"],$article);
            $error = "add";
            print "<items>\n";
              print "\t<message>".$error."</message>\n";
            print "</items>\n";
          }
        }

      }
    }
  }else if(strcmp($command,"buy") == 0){
    $db = connectToDatabase();
    for($i=0; $i<count($_SESSION["truck"]); $i++){
      $id = $db->quote($_SESSION["truck"][$i]);
      $query = "SELECT qty FROM products WHERE id = $id";
      $rows = $db->query($query);
      foreach($rows as $lines){
        $qty = $lines["qty"];
        $qty--;
        $qty = $db->quote($qty);
        $queryQty = "UPDATE products SET qty = $qty WHERE id = $id";
        $db->exec($queryQty);
      }
    }
    unset($_SESSION["truck"]);
  }
}else{
  $error = "userNotFound";
  print "<items>\n";
    print "\t<message>".$error."</message>\n";
  print "</items>\n";
}

?>
