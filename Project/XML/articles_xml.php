<?php
  if(!isset($_SESSION)){ session_start(); }
  include("../CONFIG/config.php");

  # Removes all characters except letters/numbers from query parameters
  function filter_chars($str) {
  	return preg_replace("/[^A-Za-z0-9_]*/", "", $str);
  }

  # main program
  if (!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"] != "GET") {
  	header("HTTP/1.1 400 Invalid Request");
  	die("ERROR 400: Invalid request - This service accepts only GET requests.");
  }

  $category = "";
  $all = filter_chars("tutte le categorie");

  if (isset($_REQUEST["category"])) {
  	$category = filter_chars($_REQUEST["category"]);
  }

  header("Content-type: application/xml");
  print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

  if($category) {
    print "<products>\n";

		$db = connectToDatabase();

    if(strcmp($category,$all) == 0){
      $query = "SELECT * FROM products ORDER BY RAND()";
    }else{
      $categoryQuote = $db->quote($category);
      $query = "SELECT * FROM products WHERE type = $categoryQuote ORDER BY RAND()";
    }

    $rows = $db->query($query);

    foreach($rows as $lines){
        $id = $lines["id"];
        $type_categorys = $lines["type"];
        $brand = $lines["brand"];
        $description = $lines["description"];
        $qty = $lines["qty"];
        $price = $lines["price"];
        $rating = $lines["rating"];
				$path = $lines["image"];
        $view = $lines["views"];

        if(strcmp($type_categorys,$category) == 0 || strcmp($category,$all) == 0){
        		print "\t<product id=\"$id\" category=\"$category\" brand=\"$brand\" description=\"$description\" price=\"$price\">\n";
						print "\t\t<quantity>$qty</quantity>\n";
						print "\t\t<rate>$rating</rate>\n";
						print "\t\t<path>$path</path>\n";
            print "\t\t<view>$view</view>\n";
						print "\t</product>\n";
        }
    }

	  print "</products>\n";
  }


?>
