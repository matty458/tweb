<?php
  if(!isset($_SESSION)){ session_start(); }
  include("../CONFIG/config.php");

  function filter_chars($str) {
  	return preg_replace("/[^A-Za-z0-9_]*/", "", $str);
  }

  if (!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"] != "GET") {
  	header("HTTP/1.1 400 Invalid Request");
  	die("ERROR 400: Invalid request - This service accepts only GET requests.");
  }

  $id_article = "";

  if (isset($_REQUEST["id_article"])) {
  	$id_article = filter_chars($_REQUEST["id_article"]);
  }

  header("Content-type: application/xml");
  print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

  if($id_article){
    print "<articles>\n";
    $db = connectToDatabase();
    $id_article = $db->quote($id_article);
    $query = "SELECT * FROM products WHERE id = $id_article";
    $rows = $db->query($query);

    foreach($rows as $lines){
      $id = $lines["id"];
      $type_categorys = $lines["type"];
      $brand = $lines["brand"];
      $description = $lines["description"];
      $qty = $lines["qty"];
      $price = $lines["price"];
      $rating = $lines["rating"];
      $path = $lines["image"];
      $view = $lines["views"];

      print "\t<article id=\"$id\" category=\"$type_categorys\" brand=\"$brand\" description=\"$description\" price=\"$price\">\n";
      print "\t\t<quantity>$qty</quantity>\n";
      print "\t\t<rate>$rating</rate>\n";
      print "\t\t<path>$path</path>\n";
      print "\t\t<view>$view</view>\n";
      print "\t</article>\n";

    }

    print "</articles>\n";
  }

?>
